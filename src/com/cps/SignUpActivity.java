package com.cps;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.cps.LoginActivity.Result;
import com.cps.api.ApiConfig;
import com.cps.api.ApiRequest;
import com.cps.cripto.MD5;
import com.cps.db.UserDataSource;
import com.cps.db.UserEntity;
import com.google.gson.Gson;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class SignUpActivity extends Activity {

	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private UserRegisterTask mAuthTask = null;

	// UI references.
	private AutoCompleteTextView mEmailView;
	private EditText mUsernameView;
	private EditText mPasswordView;
	private View mRegisterFormView;
	private Button btnLogin;
	private Spinner spnUserType;

	public ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_up);
		progressDialog = new ProgressDialog(this);
		progressDialog.setCancelable(false);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

		mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
		mUsernameView = (EditText) findViewById(R.id.username);
		mPasswordView = (EditText) findViewById(R.id.password);
		spnUserType = (Spinner) findViewById(R.id.user_type);

		btnLogin = (Button) findViewById(R.id.btn_login_activity);
		btnLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getApplicationContext(),
						LoginActivity.class);
				startActivity(intent);
				finish();
			}
		});

		Button mEmailSignInButton = (Button) findViewById(R.id.sign_up_button);
		mEmailSignInButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				attemptLogin();
			}
		});

		mRegisterFormView = findViewById(R.id.register_form);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sign_up, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		mUsernameView.setError(null);
		mEmailView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		String username = mUsernameView.getText().toString();
		String email = mEmailView.getText().toString();
		String password = mPasswordView.getText().toString();
		String userType = spnUserType.getSelectedItem().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password, if the user entered one.
		if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(email)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		} else if (!isEmailValid(email)) {
			mEmailView.setError(getString(R.string.error_invalid_email));
			focusView = mEmailView;
			cancel = true;
		}

		// check for a valid username
		if (TextUtils.isEmpty(username)) {
			mUsernameView.setError(getString(R.string.error_invalid_username_length));
			focusView = mUsernameView;
			cancel = true;
		}else if(!isUsernameValid(username)){
			mUsernameView.setError(getString(R.string.error_invalid_username_format));
			focusView = mUsernameView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mAuthTask = new UserRegisterTask(username, email, password,
					userType);
			mAuthTask.execute();
		}
	}

	private boolean isEmailValid(String email) {
		// TODO: Replace this with your own logic
		return email.contains("@");
	}

	private boolean isPasswordValid(String password) {
		// TODO: Replace this with your own logic
		return password.length() >= 6;
	}

	private boolean isUsernameValid(String username) {
		if (username.length() >= 6) {
			return username.matches("[\\w]*");
		} else {
			return false;
		}
	}

	public class UserRegisterTask extends AsyncTask<String, String, Boolean> {

		private final String mUsername;
		private final String mEmail;
		private final String mPassword;
		private final String mUserType;
		private Result result;

		public UserRegisterTask(String username, String email, String password,
				String userType) {
			this.mUsername = username;
			this.mEmail = email;
			this.mPassword = password;
			this.mUserType = userType;
		}

		@Override
		protected Boolean doInBackground(String... params) {
			// TODO Auto-generated method stub
			List<NameValuePair> data = new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair("username", mUsername));
			data.add(new BasicNameValuePair("email", mEmail));
			data.add(new BasicNameValuePair("password", MD5.encrypt(mPassword)));
			data.add(new BasicNameValuePair("user_type", mUserType));
			String response = ApiRequest.makeHttpRequest(
					ApiConfig.GetSignUpUrl(), ApiRequest.POST, data);
			if (response.equals("")) {
				ApiRequest.LOG = "Error connecting to server";
				return false;
			} else {
				Gson gson = new Gson();
				try {
					result = gson.fromJson(response, Result.class);
					if (result.status) {
						result.data.setHash(MD5.encrypt(mEmail
								+ MD5.encrypt(mPassword)));
						UserDataSource userDatasource = new UserDataSource(
								getApplicationContext());
						userDatasource.open();
						userDatasource.save(result.data);
						userDatasource.close();
					} else {
						return false;
					}
				} catch (Exception e) {
					ApiRequest.LOG = "Error connecting to server";
					Log.e("RestApi", e.getMessage());
					return false;
				}
			}
			return true;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			progressDialog.setMessage("Signing up ...");
			progressDialog.show();
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			progressDialog.dismiss();

			if (success) {
				Toast.makeText(getApplicationContext(), "Success",
						Toast.LENGTH_SHORT).show();
			} else {
				if (result != null) {
					for (int i = 0; i < result.message.length; i++) {
						if(result.message[i].label.equals("username"))
						{
							mUsernameView.setError(result.message[i].message);
							mUsernameView.requestFocus();
						}else if(result.message[i].label.equals("email"))
						{
							mEmailView.setError(result.message[i].message);
							mEmailView.requestFocus();
						}
					}
				} else {
					Toast.makeText(getApplicationContext(), ApiRequest.LOG,
							Toast.LENGTH_SHORT).show();
				}

			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			progressDialog.dismiss();
		}
	}

	public class Result {
		public boolean status;
		public UserEntity data;
		public Message[] message;
	}
	
	public class Message{
		public String label;
		public String message;
	}
}
