package com.cps.db;

public class UserEntity {
	
	private int id;
	private String username;
	private String email;
	private String user_type;
	private String hash;

	public UserEntity() {
		// TODO Auto-generated constructor stub
		this.id = -1;
		this.username = "";
		this.email = "";
		this.user_type = "";
	}
	
	public UserEntity(int id, String username, String email, String user_type)
	{
		this.id = id;
		this.username = username;
		this.email = email;
		this.user_type = user_type;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return this.id;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getUsername() {
		return this.username;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public void setUserType(String user_type) {
		this.user_type = user_type;
	}
	
	public String getUserType() {
		return this.user_type;
	}
	
	public void setHash(String hash) {
		this.hash = hash;
	}
	
	public String getHash() {
		return this.hash;
	}
}
