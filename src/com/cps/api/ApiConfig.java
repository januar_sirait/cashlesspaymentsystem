package com.cps.api;

public class ApiConfig {
	public static final String SERVER = "http://192.168.137.1/api/";
	public static final String LOGIN = "user/login";
	public static final String SIGN_UP = "user/signup";
	
	public static String GetLoginUrl() {
		return ApiConfig.SERVER + ApiConfig.LOGIN;
	}
	
	public static String GetSignUpUrl() {
		return ApiConfig.SERVER + ApiConfig.SIGN_UP;
	}
}
