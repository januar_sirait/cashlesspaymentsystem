package com.cps.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.util.Log;

public class ApiRequest {
	static InputStream is = null;
	static JSONObject jObj = null;
	static String json = "";

	public static final int GET = 0;
	public static final int POST = 1;
	public static String LOG = "";

	// function get json from url
	// by making HTTP POST or GET mehtod
	public static String makeHttpRequest(String url, int method,
			List<NameValuePair> params) {
		try {

			// check for request method
			if (method == POST) {
				// request method is POST
				// defaultHttpClient
				DefaultHttpClient httpClient = new DefaultHttpClient();
				HttpPost httpPost = new HttpPost(url);
				httpPost.setEntity(new UrlEncodedFormEntity(params));

				HttpResponse httpResponse = httpClient.execute(httpPost);
				HttpEntity httpEntity = httpResponse.getEntity();
				is = httpEntity.getContent();

			} else if (method == GET) {
				// request method is GET
				DefaultHttpClient httpClient = new DefaultHttpClient();
				String paramString = URLEncodedUtils.format(params, "utf-8");
				url += "?" + paramString;
				HttpGet httpGet = new HttpGet(url);

				HttpResponse httpResponse = httpClient.execute(httpGet);
				HttpEntity httpEntity = httpResponse.getEntity();
				is = httpEntity.getContent();
			}
			
			try {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(is));
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line);
				}
				is.close();
				json = sb.toString();
			} catch (Exception e) {
				Log.e("Buffer Error", "Error converting result " + e.toString());
				LOG = e.getMessage();
			}
			
		} catch (UnsupportedEncodingException e) {
			Log.e("RestAPI", e.getMessage());
			LOG = e.getMessage();
		} catch (ClientProtocolException e) {
			Log.e("RestAPI", e.getMessage());
			LOG = e.getMessage();
		} catch (IOException e) {
			Log.e("RestAPI", e.getMessage());
			LOG = e.getMessage();
		}

		// return JSON String
		return json;

	}
}
